<!-- 
*********************************************
Author Name : Sukanth Gunda 
Version : 1.0
File Name : shippingItems.jsp
*********************************************
 -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Shipping Items</title>
<style type="text/css">
table {
	border-collapse: collapse;
	width: 100%;
}

th, td {
	text-align: left;
	padding: 8px;
}

tr:nth-child(even) {
	background-color: #f2f2f2
}
tr:nth-child(odd) {
	background-color: lightgreen;
}
.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 4px 2px;
    cursor: pointer;
}
</style>
</head>
<body bgcolor="#C1E1A6">
	<c:if test="${displayFlag eq 'getItemsButton'}">
		<div align="center">
			<h1 style="font-family: fantasy;">Check the items that you want to include in this shipment</h1>
		</div>
		<hr/>
	</c:if>
	<form action="ShipmentServlet" method="post">
		<div style="padding-top: 1%">
			<c:if test="${displayFlag eq 'getItemsButton'}">
				<table border="1">
					<c:forEach var="inventory" items="${inventory}"
						varStatus="inventoryNumberIndex">
						<tr>
							<td><input type="checkbox" name="inventoryId"
								value="${inventory.pCode}|${inventory.pDescription}|${inventory.pPrice}|${inventory.pWeight}|${inventory.pShippingMethod}|${inventory.pShippingCost}"></td>
							<td><c:out value="${inventory.pCode}" /></td>
							<td><c:out value="${inventory.pDescription}" /></td>
							<td><c:out value="${inventory.pPrice}" /></td>
							<td><c:out value="${inventory.pWeight}" /></td>
							<td><c:out value="${inventory.pShippingMethod}" /></td>
							<td><c:out value="${inventory.pShippingCost}"></c:out>
						</tr>
					</c:forEach>
				</table>
			</c:if>
			<c:if test="${displayFlag eq 'getItemsButton'}">
				<div style="padding-top: 2%;padding-left: 40%">
					<input type="submit" name="createShipment" value="Create Shipment" class="button"/>
				</div>
			</c:if>
		</div>
		<div>
			<table border="1">
				<c:forEach var="selectedShipments" items="${selectedShipments}"
					varStatus="selectedShipmentsIndex">
					<tr>
						<td><c:out value="${selectedShipments}"></c:out></td>
					<tr>
				</c:forEach>
			</table>
			<c:if test="${displayFlag eq 'createShipment'}">
				<div style="padding-top: 2%;padding-left: 40%">
					<input type="submit" name="goBackToHome" value="Home" class="button"/>
				</div>
			</c:if>
		</div>
	</form>
</body>
</html>