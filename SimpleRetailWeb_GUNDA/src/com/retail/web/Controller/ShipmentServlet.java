package com.retail.web.Controller;
/**
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : com.retail.web.Controller.ShipmentServlet.java
 *
 */
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.retail.web.Beans.Item;
import com.retail.web.Service.Product;

public class ShipmentServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private static Product product = null;
	private static ArrayList<Item> allInventory = null;
	private static ArrayList<String> selectedShipments = null;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	} 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			if(request.getParameter("getItemsButton")!=null) {
				product = new Product();
				allInventory = product.getAllInventory();
				request.setAttribute("inventory", allInventory);
				request.setAttribute("displayFlag", "getItemsButton");
				request.getRequestDispatcher("/shippingItems.jsp").forward(request, response);
			}
			else if(request.getParameter("createShipment")!=null) {
				String[] id = request.getParameterValues("inventoryId");
				selectedShipments =  new ArrayList<String>();
				for(String ids:id) {
					ids = ids.replace("|", " ");
					selectedShipments.add(ids);
				}
				request.setAttribute("selectedShipments", selectedShipments);
				request.setAttribute("displayFlag", "createShipment");
				request.getRequestDispatcher("/shippingItems.jsp").forward(request, response);
			}
			else if(request.getParameter("goBackToHome")!=null) {
				selectedShipments.clear();
				request.getRequestDispatcher("/index.html").forward(request, response);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
