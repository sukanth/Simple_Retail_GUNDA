package com.retail.web.Service;

import java.util.Comparator;

import com.retail.web.Beans.Item;
/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : com.retail.web.Service.ShippingMethodComparator
 *
 */
public class ShippingMethodComparator implements Comparator<Item> {

	@Override
	public int compare(Item o1, Item o2) {
		Item item1 = (Item)o1;
		Item item2 = (Item)o2;
			return item1.getpShippingMethod().toString().compareTo(item2.getpShippingMethod().toString());
		}
}
