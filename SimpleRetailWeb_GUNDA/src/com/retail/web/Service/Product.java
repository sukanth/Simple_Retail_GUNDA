package com.retail.web.Service;

import java.util.ArrayList;
import java.util.Collections;
import com.retail.web.Beans.Item;
import com.retail.web.Beans.Item.shippingMethod;
/**
 * 
 * @author Sukanth Gunda
 * @version 1.0
 * File Name : com.retail.web.Service.Product.java
 *
 */
public class Product {
	private static ArrayList<Item>items = null; 
	private static double shippingCost;
	private static String pCodeToString = null;
	private static double shipping;
	
	public ArrayList<Item> getAllInventory(){
		/*Adding items to the list and returning the list*/
		items = new ArrayList<>();
		
		items.add(new Item(567321101987l, "CD - Pink Floyd, Dark Side Of The Moon", 19.99f, 0.58f, shippingMethod.AIR));
		items.add(new Item(567321101986l, "CD - Beatles, Abbey Road", 17.99f, 0.61f, shippingMethod.GROUND));
		items.add(new Item(567321101985l, "CD - Queen, A Night at the Opera", 20.49f, 0.55f, shippingMethod.AIR));
		items.add(new Item(567321101984l, "CD - Michael Jackson, Thriller", 23.88f, 0.50f, shippingMethod.GROUND));
		items.add(new Item(467321101899l, "iPhone - Waterproof Case", 9.75f, 0.73f, shippingMethod.AIR));
		items.add(new Item(477321101878l, "iPhone -  Headphones", 17.25f, 3.21f, shippingMethod.GROUND));
		for(Item item : items) { 
			shippingCost =  calShippingCost(item.getpShippingMethod(), item.getpCode(), item.getpWeight());
			item.setpShippingCost(shippingCost);	
		}
		Collections.sort(items, new ShippingMethodComparator());
		return items;
	}
	
	/**
	 * Description : Method to calculate shipping cost
	 * @param shipMethod
	 * @return shippingCost
	 */
	public static double calShippingCost(shippingMethod shipMethod, long pCode,float pWeight) {
		long secondToLastDigtInProductCode;
		pCodeToString = new Long(pCode).toString();
		pCodeToString = pCodeToString.substring(pCodeToString.length() - 2, pCodeToString.length() - 1);

		secondToLastDigtInProductCode = Long.parseLong(pCodeToString);
		shippingCost = (shipMethod.toString().equalsIgnoreCase("AIR")) ? (pWeight * secondToLastDigtInProductCode): (pWeight * 2.5);
		shippingCost = Double.parseDouble(String.format("%.2f", shippingCost));
		return shippingCost;
	}
}
